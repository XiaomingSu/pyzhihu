PyZhihu
=======

知乎爬虫

1、搜索话题

    >>>topic = client.get_topic('python')
    >>>topic.url     
    'http://www.zhihu.com/topic/19552832'
    >>>parent_topics =  topic.get_parent_topics()
    >>>child_topics = topic.get_sub_topics()
    >>>top_answers = topic.get_top_answerer()
2、搜索问题

    >>> question = client.get_question('如何学习编程') 
    >>>question.url
    'http://www.zhihu.com/question/19578287'
3、搜索回答者

    >>>people = client.get_people('大牛')
    >>>people.url
    'http://www.zhihu.com/people/da-niu-97'