# encoding=utf-8
import requests
import json
from models import topic, question, people
from api.constant import *


class ZhiHu:
    def search(self, query, search_type='topic'):
        if not query:
            print('type anything to search')
            return
        query = ''.join(query.strip().split()).lower()
        params = {
            'token': query,
            'max_matches': 10,
            'use_similar': 0,
        }
        r = requests.get(AUTO_SEARCH_URL, params=params)
        results = json.loads(r.text)[0]
        for result in results:
            if isinstance(result, list):
                if result[0] == search_type:
                    result_name = result[1].lower()
                    if result_name == query or query in result_name:
                        maybe = result
                    else:
                        for _ in results:
                            if _[0] == search_type:
                                maybe = _
                        if not maybe:
                            return
                    if search_type == 'topic':
                        return topic.Topic(name=maybe[1], id=maybe[2])
                    if search_type == 'question':
                        return question.Question(id=maybe[3])
                    if search_type == 'people':
                        return people.People(name=maybe[2])

    def get_topic(self, query):
        t = self.search(query=query, search_type='topic')
        if t:
            return t

    def get_question(self, query):
        q = self.search(query=query, search_type='question')
        if q:
            return q

    def get_people(self, query):
        p = self.search(query=query, search_type='people')
        if p:
            return p

if __name__ == '__main__':
    c = ZhiHu()
    t = c.get_topic('python')
    if t:
        print(t.url)
    q = c.get_question('django')
    if q:
        print(q.url)

    p = c.get_people('vczh')
    if p:
        print(p.url)