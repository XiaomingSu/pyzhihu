# encoding=utf-8
from bs4 import BeautifulSoup
import requests


def retrieve_page(page_url):
    r = requests.get(page_url)
    page = BeautifulSoup(r.text)
    return page