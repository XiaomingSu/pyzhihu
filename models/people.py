# encoding=utf-8
from api.constant import BASE_URL


class People:
    PEOPLE_URL = BASE_URL+'people/'

    def __init__(self, name):
        self.name = name
        self.url = self.PEOPLE_URL + name