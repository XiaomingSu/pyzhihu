# encoding=utf-8
from api.constant import BASE_URL

class Question:
    """
    问题类
    """
    QUESTION_URL = BASE_URL+'question/'

    def __init__(self, id):
        self.id = id
        self.url = self.QUESTION_URL + str(id)
        self.page = None


