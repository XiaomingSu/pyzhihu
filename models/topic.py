# encoding=utf-8
from api.constant import TOPIC_URL
from utils.crawler import retrieve_page
from bs4 import BeautifulSoup
import requests
import sys

class Topic:
    def __init__(self, name, id):
        self.name = name
        self.id = id
        self.url = TOPIC_URL + str(id)
        self.parents = []
        self.subs = []
        self.page = None

    def __str__(self):
        return self.name

    def get_parent_topics(self):
        if not self.page:
            self.page = retrieve_page(self.url)
        parent_nodes = self.page.findAll(attrs={'class': 'parent-topic'})
        if parent_nodes:
            try:
                parents_holder = parent_nodes[0]
                parents = parents_holder.findAll('a')
                for p in parents:
                    p_topic = Topic(name=p.text, id=p['data-token'])
                    self.parents.append(p_topic)
            except:
                print(sys.exc_info())
            else:
                return self.parents

    def get_sub_topics(self):
        if not self.page:
            self.page = retrieve_page(self.url)
        child_nodes = self.page.findAll(attrs={'class': 'child-topic'})
        if child_nodes:
            try:
                children_holder = child_nodes[0]
                subs = children_holder.findAll('a')
                for s in subs:
                    s_topic = Topic(name=s.text, id=s['data-token'])
                    self.subs.append(s_topic)
            except:
                print(sys.exc_info())
            else:
                return self.subs

    def get_top_answerer(self):
        if not self.page:
            self.page = retrieve_page(self.url)
        answerer_nodes = self.page.findAll(attrs={'id': 'zh-topic-top-answerer'})
        if answerer_nodes:
            try:
                answers_holder = answerer_nodes[0]
                answerers = answers_holder.findAll('a', attrs={'class': 'zm-list-avatar-link'})
                for answerer in answerers:
                    pass
            except:
                pass



if __name__ == '__main__':
    t = Topic('python', 19552832)
    print(t.get_parent_topics())
    print(t.get_sub_topics())
